---
Week: 14
Content: 
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 14 - Sikkerhed på lag 1 og 2 **(KLADDE)**

## Emner

## Ugens emner er

- Sikkerhed på lag 1-2
- Ettercap
- Wireless

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- [Øvelse 18: ARP](../exercises/old/18_arp.md)
- [Øvelse 19: Ettercap basics](../exercises/old/19_ettercap.md)
- [Øvelse 20: Ettercap DNS](../exercises/old/20_ettercap_dns.md)
- [Øvelse 21: Wireless](../exercises/old/21_wireless.md)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  
    - Netværkstrusler
    - Trådløs sikkerhed
    - Sikkerhed i TCP/IP
    - Adressering i de forskellige lag

- **_Den studerende kan_**
    - Identificere sårbarheder som et netværk kan have. 

- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
    - Designe, konstruere og implementere samt teste et sikkert netværk 

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan forklare adressering på lag 2
- Den studerende udføre MAC spoofing 
- Den studerende kan forklare principperne for wireless sikkerhed 

## Afleveringer

Øvelser og refleksioner over læring dokumenteret på studiegruppernes gitlab pages:  

1. [byteshield](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/)
2. [cryptohex](https://24f-its-semesterprojekt-studerende.gitlab.io/2-cryptohex/)
3. [hexforce](https://24f-its-semesterprojekt-studerende.gitlab.io/3-hexforce/)
4. [secminds](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/)
5. [hexdef](https://24f-its-semesterprojekt-studerende.gitlab.io/5-hexdef/)
6. [ciphercrew](https://24f-its-semesterprojekt-studerende.gitlab.io/6-ciphercrew/)
7. [codecrypts](https://24f-its-semesterprojekt-studerende.gitlab.io/7-codecrypts/)

## Skema

### Onsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Feedback på jeres gitlab dokumentation |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- 4 lektioner med underviser, resten af dagen Gruppearbejde, øvelser og selvstudie


## Ressourcer

* 802.1x 
    * "den sædvanlige" som man bruger til Network Access Control (NAC)
    * Man forbinder til en port på switchen, logger ind, og bliver tildelt et vlan
    * Dette burde bare virke som klient med windows, mac, linux mv.
    * Authentication modes from [juniper](https://www.juniper.net/documentation/us/en/software/junos/user-access/topics/topic-map/802-1x-authentication-switching-devices.html)
        * Obs: Der er flere modes og fall back til enheder som ikke kan 802.1x
    * Lidt fra [VMware](https://www.vmware.com/topics/glossary/content/network-access-control.html)
* IEEE 802.1AE (MACsec) 
    * Krypteret trafik mellem L2 enheder.
    * From [juniper](https://www.juniper.net/documentation/us/en/software/junos/security-services/topics/topic-map/understanding_media_access_control_security_qfx_ex.html)
    * [from zindagitech](https://zindagitech.com/what-is-ieee-802-1ae-macsec-how-does-it-work/)
    * Quick intro from [Phil Anderson](https://www.youtube.com/watch?v=u-nisjfHhtQ)
* Angreb
    * DHCP, 802.1x, VLAN, MAC spoof, mv
    * [ettercap](https://www.ettercap-project.org/) er en gammel kending. Se Øvelse [19](../exercises/old/19_ettercap.md) og [20](../exercises/old/20_ettercap_dns.md).
    * Mht. Wireless ville jeg starte ved [aircrack-ng](https://www.aircrack-ng.org/). Og en [guide](https://nooblinux.com/crack-wpa-wpa2-wifi-passwords-using-aircrack-ng-kali-linux/), og en [demo](https://www.youtube.com/watch?v=uKZb3D-PHS0)

Det kan være forstyrrende, socialt uacceptabelt, brud på den lokale IT politik, og måske kriminelt at lege med denne slags. Slå hjernen til. 

