---
Week: 09
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 09 - Bygge netværk **(KLADDE)**

## Emner

Sidste uge snakkede vi om netværks topologi og hvordan man strukturerer sit netværk for at få redundans og (lidt) sikkerhed.

Denne uge ser vi på hvordan man rent praktisk indretter sig med IP adresser, routere og VLANs.

* Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/03_network_layer/)

## Ugens emner er

- Bygge netværk
- IPAM
- Netflow

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- Den studerende har en minimal Debian headless server og en openbsd router.
- [IPAM Øvelse](https://ucl-pba-its.gitlab.io/23f-its-nwsec/exercises/07_ipam/)
- [netflow Øvelse](https://moozer.gitlab.io/course-networking-security/03_network_layer/netflow/#netflow-using-ntopng)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  
    - Forskellige sniffing strategier og teknikker
    - Netværk management (overvågning/logning, snmp)
- **_Den studerende kan_**
    - Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
    - Designe, konstruere og implementere samt teste et sikkert netværk
    - Monitorere og administrere et netværks komponenter

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan designe et netværk ud fra simple specifikationer
- Den studerende kan designe et IP layout for et simpelt netværk
- Den studerende kan forklare forskellene på fysiske og virtuelle enheder (interfaces, servere, LANs)
- Den studerende kan forklare hvad netflow er og hvad det kan bruges til

## Afleveringer

Øvelser og refleksioner over læring dokumenteret på studiegruppernes gitlab pages:  

1. [byteshield](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/)
2. [cryptohex](https://24f-its-semesterprojekt-studerende.gitlab.io/2-cryptohex/)
3. [hexforce](https://24f-its-semesterprojekt-studerende.gitlab.io/3-hexforce/)
4. [secminds](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/)
5. [hexdef](https://24f-its-semesterprojekt-studerende.gitlab.io/5-hexdef/)
6. [ciphercrew](https://24f-its-semesterprojekt-studerende.gitlab.io/6-ciphercrew/)
7. [codecrypts](https://24f-its-semesterprojekt-studerende.gitlab.io/7-codecrypts/)

## Skema

### Onsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Feedback på jeres gitlab dokumentation |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- 4 lektioner med underviser, resten af dagen Gruppearbejde, øvelser og selvstudie

## Ressourcer

- none supplied