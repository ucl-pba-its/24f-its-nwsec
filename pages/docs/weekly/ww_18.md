---
Week: 18
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 18 - Sikkerhedstest af netværk

## Emner

## Ugens emner

- Red teaming
- Blue teaming
- Purple teaming
- Cyber kill chains

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- [Øvelse 110 - red teaming](../exercises/110_red_team_knowledge.md)
- [Øvelse 111 - blue teaming](../exercises/111_blue_team_knowledge.md)
- [Øvelse 112 - purple teaming](../exercises/112_purple_team_knowledge.md)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  
    - Netværkstrusler

- **_Den studerende kan_**
    - Teste netværk for angreb rettet mod de mest anvendte protokoller.
    - Identificere sårbarheder som et netværk kan have.

- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
    - Designe, konstruere og implementere samt teste et sikkert netværk.
    - Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)

#### Læringsmål den studerende kan bruge til selvvurdering

## Afleveringer

- Den studerende har viden om red, blue og purple teaming
- Den studerende har viden om cyber kill chains
- Den studerende kan planlægge sikkerhedstest af netværk
- Den studerende kan rapportere om sikkerhedstest af netværk

## Skema

### Mandag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Øvelser fra sidste uge |
| 09:30 | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K1/K4          |
| 15:30 | Fyraften               |

## Kommentarer

- Undervisning er Mandag den 29. April 2024 - 8 lektioner fra 8:15 - 15:30

## Ressourcer

- none supplied