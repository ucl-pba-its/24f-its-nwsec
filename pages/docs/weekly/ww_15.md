---
Week: 15
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 15 - ARP, DNS og Man In The Middle

## Emner

I denne uge er emnet 

## Ugens emner er

- Address Resolution Protocol (ARP)
- Domain Name System (DNS)
- Man In The Middle (MITM)

## Forberedelse

- [ARP Poisoning | Man-in-the-Middle Attack](https://youtu.be/A7nih6SANYs?si=HnKCGg0BFMJ0UnPa)
- [DNS Cache Poisoning - Computerphile](https://youtu.be/7MT1F0O3_Yw?si=asMd338AgOJHzgjZ)

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- [Øvelse 91 - Grundlæggende ARP](../exercises/91_arp.md)
- [Øvelse 92 - ARP spoofing med Ettercap](../exercises/92_ettercap_arp.md)
- [Øvelse 93 - Grundlæggende DNS](../exercises/93_dns_basics.md)
- [Øvelse 94 - Rekognosering](../exercises/94_recon.md)
- [Øvelse 95 - Ettercap DNS MITM](../exercises/95_ettercap_dns.md)
- [Øvelse 96 - Tryhackme digdug](../exercises/96_digdug.md)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  
    - Netværkstrusler
    - Forskellige sniffing strategier og teknikker

- **_Den studerende kan_**
    - Identificere sårbarheder som et netværk kan have

- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
    - Designe, konstruere og implementere samt teste et sikkert netværk

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan forklare princippet bag MITM og spoofing
- Den studerende kan udføre ARP poisoning/spoofing og DNS MITM med henblik på at teste et netværks sikkerhed

## Afleveringer

Øvelser og refleksioner over læring dokumenteret på studiegruppernes gitlab pages:  

1. [byteshield](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/)
2. [cryptohex](https://24f-its-semesterprojekt-studerende.gitlab.io/2-cryptohex/)
3. [hexforce](https://24f-its-semesterprojekt-studerende.gitlab.io/3-hexforce/)
4. [secminds](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/)
5. [hexdef](https://24f-its-semesterprojekt-studerende.gitlab.io/5-hexdef/)
6. [ciphercrew](https://24f-its-semesterprojekt-studerende.gitlab.io/6-ciphercrew/)
7. [codecrypts](https://24f-its-semesterprojekt-studerende.gitlab.io/7-codecrypts/)

## Skema

### Onsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- 4 lektioner med underviser, resten af dagen gruppearbejde, øvelser og selvstudie

## Ressourcer

