---
hide:
  - footer
---

# Øvelse 92 - ARP spoofing med Ettercap 

## Information

Ettercap er et tool til at lave forskellige typer man-in-the-middle angreb.
I denne øvelse skal du afprøve ettercap til at lave ARP cache posioning/ARP spoofing.  

Princippet i ARP spoofing er at narre en computer til at sende netværkstrafik til en "fjendtlig" maskine.  
Alle computere har en ARP tabel som er en tabel der indeholder MAC adresser og tilhørende IP adresser.  

På billedet herunder ses en maskines ARP tabel hhv. før og efter ARP poisoning fra en Kali maskine på samme netværk.  
Læg mærke til at MAC adressen der tilhører maskinens default gateway `192.168.186.1` ændres fra `00:50:56:c0:00:08` (inden poisoning) til `00:0c:29:ed:1f:9b` (efter poisoning) hvilket vil få maskinen til at sende netværkstrafik med destination `192.168.186.1` til MAC adressen `00:0c:29:ed:1f:9b`.  
"Routeren" har tilsvarende fået sin ARP tabel ændret af poisoning således at den tror maskinen `192.168.186.128` har MAC adressen `00:0c:29:ed:1f:9b` (ikke vist på billedet fordi routeren er vmware).  

![arp table poisoned](../images/ettercap_arp_poisoning_1.png)

Formålet med Man In The Middle for en trusselsaktør er at netværkstrafikken fra "victim" computeren dirigeres gennem trusselsaktørens computer. Det åbner op for at trusselsaktøren både kan "sniffe" men også manipulere netværkstrafikken.  

Øvelsen giver dig praktisk erfaring samt viden om hvad ARP spoofing er, samt hvordan det kan udføres.  
Målet er at du ved hvilke trusler det kan medføre. 

Hvis du sidder fast i øvelsen kan denne artikel være en hjælp: [Windows 10 ARP Spoofing with Ettercap and Wireshark](https://cybr.com/cybersecurity-fundamentals-archives/windows-10-arp-spoofing-with-ettercap-and-wireshark/)  

Du kan finde flere ressourcer nederst i øvelsen.  

## Instruktioner

1. Start to virtuelle maskiner på samme netværk (f.eks vmnet8) den ene skal være Kali.

    Kali maskinen kalder vi "kali" og den anden "victim"

2. Find MAC og ip adresserne på både kali og victim

    Notér dem ned

3. Start wireshark på kali

4. Start `ping 8.8.8.8` på victim

    Kan det ses i wireshark på kali?

    Hvorfor Ja? hvorfor nej?

5. Start ettercap

    Add Victim som target 1 og router/default gataway som target 2

    Start ARP poisoning

6. Start `ping 8.8.8.8` på victim

    Kan det ses i wireshark på kali?

    Hvorfor Ja? hvorfor nej? Er der noget at bemærke?

7. Prøv at logge ind fra victim på adressen `http://testphp.vulnweb.com/login.php` 

    Ser du noget i ettercap? Hvis ja, hvad?

5. Giv dine bedste bud på hvordan ARP spoofing kan mitigeres, brug f.eks [MITRE attack](https://attack.mitre.org/tactics/TA0009/)
8. Opsamling på klassen - en gruppe præsenterer

## Ressourcer

- [Ettercap MAN pages](https://linux.die.net/man/8/ettercap)
- [Data link layer](https://en.wikipedia.org/wiki/Data_link_layer)
- [Network switch](https://en.wikipedia.org/wiki/Network_switch)
- [MAC tabel](https://en.wikipedia.org/wiki/MAC_table)
- [ARP](https://en.wikipedia.org/wiki/Address_Resolution_Protocol)
