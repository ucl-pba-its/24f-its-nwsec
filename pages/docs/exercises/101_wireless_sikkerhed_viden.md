---
hide:
  - footer
---

# Øvelse 101 - Trådløs sikkerhed viden

## Information

Dette er en gruppe øvelse.

Trådløse netværk er ofte svære at begrænse til en fysisk lokation og det åbner for at uautoriserede enheder kan forbinde til et trådløst netværk fra f.eks en parkeringsplads eller hotspot på din telefon.   
Derfor er det nødvendigt at sikre adgangen til trådløse netværk. 

I denne øvelse skal i undersøge nøglebegreber og standarder der hører til trådløs sikkerhed.

Husk at bruge nedenstående ressourcer, suppler gerne med egen research.

## Instruktioner

1. Hvad er et SSID?
2. Hvad er WEP, WPA, WPA2 og WPA3? Regnes de alle for sikre?
3. Hvad er forskellen på PSK og IEEE 802.1X/EAP autentificering?    
4. Hvad er formålet med Wi-Fi 4 way handshake? Kan det misbruges? Hvis ja, hvordan?
5. Hvad er et PMKID interception attack? Beskriv med egne ord hvordan det fungerer 
6. Hvad er en lavpraktisk måde at beskytte netværk med PSK?

## Ressourcer

- [Video: Wi-Fi 4-Way Handshake In Depth](https://youtu.be/vHIRmG_BzQI?si=s31gILcHYepLlHLz)
- [Extensible Authentication Protocol](https://en.wikipedia.org/wiki/Extensible_Authentication_Protocol)
- [https://www.comparitech.com/blog/information-security/wpa2-aes-tkip/](https://www.comparitech.com/blog/information-security/wpa2-aes-tkip/)
- [Video: Krack Attacks (WiFi WPA2 Vulnerability) - Computerphile](https://youtu.be/mYtvjijATa4?si=cnS5Y9GU0gE6H58H)
- [PMKID interception](https://www.kaspersky.com/blog/wi-fi-pmkid-attack/50790/)