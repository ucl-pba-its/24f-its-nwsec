---
hide:
  - footer
---

# Øvelse 112 - Purple teaming

## Information

Purple teaming har jeg ikke kunne finde en oprindelse på (kan i?).  
I de ressourcer jeg har kigget i er der dog enigehed om at der ikke findes et decideret purple team. Purple teaming beskriver et samarbejde mellem red team og blue team med formålet at have fælles mål og en fælles forståelse af sikkerhed, fremfor en udelukkende offensiv eller defensiv forståelse.  

Ved traditionel red vs. blue øvelser kan fokus ofte ende med at være at enten red eller blue team vandt. Det er ikke særlig brugbart for en virksomhed. Her hjælper purple teaming med at ensrette indsatsen fra de to teams så de har det samme mål: At styrke sikkerheden.  

### Hvordan arbejder purple teams?

Purple teams er som nævnt et samarbejde mellem blue og red team.  
Det kan i større organisationer være et kontinuerligt samarbejde mellem de to teams men er ofte et samarbejde omkring deciderede purple team øvelser.  

På itslearning har jeg kopieret kapitel 1 og 2 fra bogen [Purple Team Strategies](https://www.packtpub.com/product/purple-team-strategies/9781801074292) som i bør bruge som supplement til det jeg skriver her.

På side 30-33 beskrives en generisk model for purple teaming øvelser. Modellen har 4 faser: Prepare, Execute, Identify og Remediate. 
Udover blue tema og red team er der en purple team manager til at koordinere øvelsen.  

Jeg vil herunder give en kort og forsimplet opsummering af hvad de 4 faser indeholder:

1. Prepare  

    Alle medlemmer fra både blue og red team deltager.  
    Her udvælges hvilke sikkerheds test der skal udføres, enten manuelt eller automatiseret.   
    En trusslesaktør samt relevante TTP udvælges som red team skal forberede sig på at kunne udføre og blue team skal forberede sig på at kunne forsvare mod. 

2. Execute  

    Red team udfører angreb og blue team detekterer og forsvarer.  
    Blue team rapporterer det de observerer til purple team manager. 

3. Identify  

    Alle medlemmer fra både blue og red team deltager.
    Øvelsen gennemgås med henblik på at afdække hvor effektiv sikkerheden der blev afprøvet er. Helt simpelt, kunne blue team detektere og forsvare mod de angreb red team udførte?  
    Alle sikkerhedsbrister vurderes og prioriteres i forhold til hvor meget en mitigering kan reducere risikoen (hvor meget hjælper det?) og hvor mange ressourcer en mitigering indebærer (hvad koster det?).  
    Purple team manager dokumenterer processen.

4. Remediate  

    Blue team implementere de tiltag der reducerer risikoen mest og kræver færrest ressourcer.  
    Red team gentager de tilhørende TTP for at validere at de nu afvises af de nye sikkerhedstiltag.  
    Blue team dokumenterer og planlægger, sammen med purple team manager, de resterende svagheder samt planlægger hvornår deres mitigering skal implementeres. 

Purple team øvelser kan udføres på tidspunkter der er kendt af blue team eller uden blue teams kendskab.  
Hvis det ikke er kendt af blue team er formålet at måle responstiden før for eksempel et angreb opdages. 
Her anvendes to metrikker, Mean Time to Detect (MTTD) og Mean Time to Respond (MTTR).  

Der er mange fordele ved at arbejde med purple teaming. En af de største i min optik er den øgede forståelse for hvordan hhv. blue og red team tænker og arbejder samt den øgede kommunikation og fokus på samme mål.  

## Instruktioner

1. Lav en brainstorm i jeres gruppe over hvordan i kan bruge purple teaming i jeres semester projekt?
    - Fra side 36-47 i [Purple Team Strategies](https://www.packtpub.com/product/purple-team-strategies/9781801074292) beskrives forskellige purple teaming øvelser, brug dem som inspiration
    - Som rapportering kan i tage udgangspunkt i [Purple teaming report](../other_docs/Purple_Teaming_Report_v1.0.xlsx)
    - Brug [Purple team exercise framework](https://github.com/scythe-io/purple-team-exercise-framework/) som inspiration
2. Skriv resultatet af drøftelserne i jeres dokumentation
3. Forbered jer på at præsentere resultaterne på klassen (vi tager en kort runde).

## Ressourcer

- [A Guide to Purple Teaming](https://medium.com/@milos.s/a-guide-to-purple-teaming-what-why-who-when-how-1c00184e01da)
- [Purple team exercise framework](https://github.com/scythe-io/purple-team-exercise-framework/)
- [Purple teaming report](../other_docs/Purple_Teaming_Report_v1.0.xlsx)
- [THM: Cyber Defence Frameworks](https://tryhackme.com/module/cyber-defence-frameworks)
- [THM: Intro to Threat Emulation ](https://tryhackme.com/r/room/threatemulationintro)
- [BOG: Purple Team Strategies](https://www.packtpub.com/product/purple-team-strategies/9781801074292)
- [BOG: Purple team field manual](https://a.co/d/aP3h6S0)