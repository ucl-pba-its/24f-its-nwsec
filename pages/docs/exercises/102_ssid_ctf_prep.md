---
hide:
  - footer
---

# Øvelse 102 - Trådløs CTF forberedelse

## Information

Dette er en gruppe øvelse.

I denne øvelse skal i forberede jer til den trådløse CTF. I skal installere en Wi-Fi USB adapter ([tp-link archer T2U plus](https://www.tp-link.com/us/home-networking/usb-adapter/archer-t2u-plus/)) samt teste at den virker i Kali Linux på VMWare (i kan også anvende virtualbox men jeg har ikke afprøvet det selv).

I skal også undersøge et par værktøjer som kan bruges til at penteste Wi-Fi.  

## Instruktioner

1. Start kali i VMWare workstation
2. Tilslut wireless adapter f.eks tp-link archer T2U 
3. Installer driver i kali `sudo apt install realtek-rtl88xxau-dkms` (brug evt links herunder)

    ```bash
    sudo apt udate
    sudo apt install realtek-rtl88xxau-dkms
    reboot
    dkms status
    sudo dkms build realtek-rtl88xxau/
    sudo apt install linux-headers-6.6.15-amd64
    sudo dkms build realtek-rtl88xxau/
    ip a
    sudo dkms build realtek-rtl88xxau/5.6.4.2~git20231117.a3e0c0b --force
    reboot
    ```

5. Alternativt kan Parrot security bruges i stedet for Kali [https://www.parrotsec.org/download/](https://www.parrotsec.org/download/)
4. Test at i kan forbinde fra kali til hotspot på en mobiltelefon
5. Gennemfør i gruppen [https://tryhackme.com/r/room/wifihacking101](https://tryhackme.com/r/room/wifihacking101)
6. Undersøg disse værktøjer (brug evt links herunder)
    - aircrack
    - airgeddon
    - wifite
7. Test [monitor mode](https://itigic.com/what-is-the-monitor-mode-in-wifi-cards-or-adapters/) med valgt værktøj, hvis i kan se wifi data fra forskellige netværk er i klar til CTF. 

## Ressourcer

- Archer T2U plus driver install
    - [https://forums.kali.org/showthread.php?70325-Trouble-getting-Kali-to-recognise-USB-wireless-adapter-in-virtual-machine](https://forums.kali.org/showthread.php?70325-Trouble-getting-Kali-to-recognise-USB-wireless-adapter-in-virtual-machine)
- aircrack 
    - [https://www.aircrack-ng.org/doku.php?id=newbie_guide](https://www.aircrack-ng.org/doku.php?id=newbie_guide)
    - [https://www.kali.org/tools/aircrack-ng/](https://www.kali.org/tools/aircrack-ng/)
- airgeddon 
    - [https://www.hackingarticles.in/wireless-penetration-testing-airgeddon/](https://www.hackingarticles.in/wireless-penetration-testing-airgeddon/)
    - [https://www.kali.org/tools/airgeddon/](https://www.kali.org/tools/airgeddon/)
- wifite
    - [https://www.hackershousenepal.com/2020/12/wifite-wifi-wireless-hacking-tutorial-pentest-guide.html](https://www.hackershousenepal.com/2020/12/wifite-wifi-wireless-hacking-tutorial-pentest-guide.html)
    - [https://medium.com/@govindsharma606040/cracking-wifi-wpa2-password-using-hashcat-and-wifite-85cd024d9c83](https://medium.com/@govindsharma606040/cracking-wifi-wpa2-password-using-hashcat-and-wifite-85cd024d9c83)
    - [https://www.kali.org/tools/wifite/](https://www.kali.org/tools/wifite/)