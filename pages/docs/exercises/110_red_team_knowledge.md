---
hide:
  - footer
---

# Øvelse 110 - Red teaming

## Information

Red teaming er en offensiv diciplin hvor en eller flere personer emulerer en trusselsaktør, med henblik på at afprøve en virksomheds forsvar mod denne.  

Red team og blue team stammer så vidt jeg kan finde ud af fra [Lieutenant von Reisswitz’s Kriegsspiel](https://www.armchairdragoons.com/articles/research/nineteenth-century-military-war-games-lieutenant-von-reisswitzs-kriegsspiel/) hvor brikkerne i spillet var farvet hhv. røde (modstander) og blå (forsvar).

### Hvordan arbejder et red team

Red teaming og penetration testing bruges ofte til at forklare hvordan etiske hackere prøver at bryde ind i en virksomhed. De to ting er dog ikke det samme. Penetration testing er primært en teknisk diciplin hvor en firewall, api eller andet teknisk system afprøves.  

Et red team er mere omfattende og bruger også ikke tekniske metoder som social engineering til at afprøve en virksomheds sikkerhed.  
Et red team vil ofte emulere (efterligne) en trusselsaktørs teknikker, taktikker og procedurer (TTP) og på den måde kunne hjælpe virksomheden med at afprøve den type trussel/angreb som en specifik trusselsaktør kan udsætte dem for.   

Et eksempel på en trusselsaktør kan være den russiske gruppe "Sandworm" som angreb den danske energi sektor i 2023.  
Angrebet er beskrevet i en rapport som [SektorCERT](https://sektorcert.dk/) (de kritiske sektorers cybersikkerhedscenter) har udgivet.  
Du kan hente rapporten her: [Angrebet mod dansk, kritisk infrastruktur](https://sektorcert.dk/wp-content/uploads/2023/11/SektorCERT-Angrebet-mod-dansk-kritisk-infrastruktur-TLP-CLEAR.pdf)  

I rapporten nævnes trusslesaktøren Sandworm. Sandworm er en såkaldt Advanced Persistent Threat (APT) gruppe og anses som statsstøttet af den russiske stat.   
Du kan læse mere om sandworm her [https://attack.mitre.org/groups/G0034/](https://attack.mitre.org/groups/G0034/)
APT grupper er kendetegnet ved at have mange ressourcer og meget tid til at udvikle deres TTP.  

Red teams kan være interne (typisk større virksomheder) eller eksterne (konsulent virksomheder)  
Et eksternt red team arbejder altid efter aftale som er nedskrevet i en kontrakt. Her er det tydeligt afgrænset hvilke ip adresser, servere, konti, fysiske lokationer osv. som red team må afprøve.  
Kontrakten er selvfølgelig til for at beskytte red team så de ikke kan anklages for at have gjort noget ulovligt, men også fordi der hos virksomheden sidder et blue team der har implementeret foranstaltninger som skal afprøves.  

I kontrakten er det også aftalt hvilke mål øvelsen har. Et mål kan være at få adgang til en specifik host på netværket eller at hente data fra en specifik database. Du kan sammenligne mål med de flag i finder i en CFT øvelse. 

Det er forskelligt hvad red team afprøver afhængigt af det behov som der er i den givne situation.  
Det kan være et fuldt angreb hvor red team afprøver en fuld attack path, fra initial adgang til opnåelse af alle mål.  
Red team kan også starte fra udgangspunktet at den initiale adgang er opnået og de dermed skal forsøge at opnå yderligere adgang på netværket.  

#### Cyber kill chains

De faser som en trusselaktør gennemgår i et angreb er forsøgt standardiseret i forskellige "killchains".  

Her kan nævnes for eksempel cyber kill chain fra [Lockheed Martin](https://www.lockheedmartin.com/en-us/capabilities/cyber/cyber-kill-chain.html) eller [Unified kill chain](https://unifiedkillchain.com/).  
[Mitre Att&ck](https://attack.mitre.org/) er også opdelt i angrebsfaser, kaldet taktikker (tactics).

1. [TA0043 Reconnaissance](https://attack.mitre.org/tactics/TA0043/) - Trusselsaktøren indhenter informationer der kan bruges til angreb. 
2. [TA0042 Resource Development](https://attack.mitre.org/tactics/TA0042/) - Opnåelse af konti, servere og andre ressourcer som kan bruges til at understøtte angrebet.
3. [TA0001 Initial Access](https://attack.mitre.org/tactics/TA0001/) - Den første adgang til netværket, som regel med få privilegier og begrænset adgang til andre dele af netværket
4. [TA0002 Execution](https://attack.mitre.org/tactics/TA0002/) - Afvikling af ondsindet kode med formålet at opnå yderligere adgang eller udvidede privilegier
5. [TA0003 Persistence](https://attack.mitre.org/tactics/TA0003/) - En permanent adgang til netværket etableres så ændring af passwords etc. ikke fjerner trusselsaktørens adgang
6. [TA0004 Privilege Escalation](https://attack.mitre.org/tactics/TA0004/) - Opnåelse af større privilegier, for eksempel administrator rettigheder. 
7. [TA0005 Defense Evasion](https://attack.mitre.org/tactics/TA0005/) - Teknikker der har det formål at trusselsaktøren ikke bliver opdaget på netværket
8. [TA0006 Credential Access](https://attack.mitre.org/tactics/TA0006/) - Indhentning af legitime konti. Kan give adgang og er svære at detektere.
9. [TA0007 Discovery](https://attack.mitre.org/tactics/TA0007/) - afdækning af systemer og netværk (miljøet).
10. [TA0008 Lateral Movement](https://attack.mitre.org/tactics/TA0008/) - opnåelse af adgang til yderligere dele af netværket/systemet.
11. [TA0009 Collection](https://attack.mitre.org/tactics/TA0009/) - Indsamling af data der kan brugers af f.eks en ransomware gruppe til afpresning.
12. [TA0011 Command and Control](https://attack.mitre.org/tactics/TA0011/) - Kommunikation med kompromiterede systemer fra trusslesaktørens egen systemer, ofte en [C2 server](https://www.sentinelone.com/cybersecurity-101/what-are-command-control-c2-servers/)
13. [TA0010 Exfiltration](https://attack.mitre.org/tactics/TA0010/) - Komprimering, kryptering og overførsel af det indsamlede data
14. [TA0040 Impact](https://attack.mitre.org/tactics/TA0040/) - forstyrrelse eller ødelæggelse af systemer og data med forskellige formål.

For hver taktik findes et antal teknikker (techniques) og tilhørende sub teknikker (sub techniques).  
[Mitre attack navigator](https://mitre-attack.github.io/attack-navigator/) giver mulighed for at søge på en APT gruppe og få vist hvilke teknikker de historisk har anvendt.   

Som eksempel har Sandworm brugt teknikken [T1040 Network Sniffing](https://attack.mitre.org/techniques/T1040/) til at [angribe Ukraine i 2015](https://attack.mitre.org/campaigns/C0028).  
Her brugte de værktøjet [S0089 BlackEnergy](https://attack.mitre.org/software/S0089/) til at sniffe brugernavne og passwords mellem det interne LAN og elnettets industrielle kontrol systemer (ICT). 

Red team kan bruge attack navigator til at planlægge deres aktiviteter, så de ligner dem en specifik trusselsaktør anvender. For eksempel kan det være aktuelt for den danske energisektor at lave en red team øvelse baseret på nogle af Sandworms taktikker og teknikker med henblik på at afprøve forsvaret mod den trusselsaktør. 

#### Rapportering

Rapportering er en stor del af et red team´s arbejde. Det er rapporteringen der giver værdi til red team arbejdet ved at kunden får håndgribelige beviser på hvordan de er sårbare og hvilke adgange red team har opnået.  

Det vil sige at der er meget skrive arbejde som red teamer (og som pentester).  
Alt skal dokumenteres i et omfang som gør det muligt for kundens blue team at implementere foranstaltninger mod samme type angreb i fremtiden.  
Hvordan en rapport ser ud vil være afhængigt af den virksomhed som udfører red team øvelsen. De vil have en skabelon og et specifikt sprog som der anvendes i kommunikation med kunden.  

Nogle gange vil en eller flere fra red team også besøge kunden og præsentere resultaterne. Her vil udover red team ofte være en repræsentant fra kundens ledelse samt en eller flere tekniske medarbejdere.   

Jeg har fundet et eksempel på en red team rapport som i kan se her:  
[redteam.guide: Red team report template](https://redteam.guide/docs/Templates/report_template)

## Instruktioner

Nu ved i lidt om red teaming, der er selvfølgelig mange flere detaljer som i kan dykke ned i. For eksempel kigge nærmere på forskellige APT grupper eller flere taktikker og teknikker fra MITRE ATT&CK.  

Jeres opgave er nu i gruppen at tale om hvordan red teaming kan bruges i jeres eksamensprojekt.   

1. Lav en brainstorm i jeres gruppe over hvordan i kan bruge red teaming i jeres semester projekt? Her er lidt inspiration til at komme i gang med snakken.
    - Kig i [Mitre Att&ck](https://attack.mitre.org/): Hvilke taktikker og teknikker kan i afprøve på jeres proxmox installation
    - Hvordan kan i beskrive en red team øvelse i jeres eksamensrapport?
    - Er det aktuelt for jer at bruge for eksempel [Unified kill chain](https://unifiedkillchain.com/) og i så fald hvordan?
2. Skriv resultatet af drøftelserne i jeres dokumentation.
3. Forbered jer på at præsentere resultaterne på klassen (vi tager en kort runde mellem grupperne).

## Ressourcer

- [Cyber Kill Chain, MITRE ATT&CK, and Purple Team](https://www.sans.org/blog/cyber-kill-chain-mitre-attack-purple-team/)
- [redteam.guide: Red team report template](https://redteam.guide/docs/Templates/report_template)
- [THM: Red Team Fundamentals](https://tryhackme.com/r/room/redteamfundamentals)
- [BOG: Red team field manual](https://a.co/d/615IG5D)
- [VIDEO: RedTeaming VS PenTesting](https://youtu.be/23jIaxkh0pI?si=6fob-Jn2BHXILL4h)
