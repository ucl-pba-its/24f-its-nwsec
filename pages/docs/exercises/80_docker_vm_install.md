---
hide:
  - footer
---

# Øvelse 80 - Virtuel Docker host 

## Information

Dette er en gruppeøvelse.  

I denne øvelse skal i udvide jeres proxmox installation med en virtuel maskine der har docker installeret.
Jeg anbefaler i bruger Ubuntu server uden desktop installeret.

Herunder kan i se at docker hosten skal være på `MONITOR` subnettet.

![Lab diagram 20240319 - NISI](../images/lab_diagram_20240319.png)

## Instruktioner

1. Lav en ny VM i proxmox og konfigurer den som vist herunder:
    ![Proxmox docker vm config](../images/proxmox_docker_vm_config.png)
2. Brug MAC adressen fra den virtuelle maskine til at lave en statisk lease i DHCP på `MONITOR` netværket i opnsense. 
    ![Proxmx docker ip](../images/proxmox_docker_vm_ip.png)
2. Installer docker via denne guide [https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)
3. Dokumenter opsætning af ip og docker på gitlab.

## Ressourcer

- [Docker dokumentation](https://docs.docker.com/)




