---
hide:
  - footer
---

# Øvelse 82 - Graylog og firewall regler 

## Information

Dette er en gruppeøvelse. Øvelsen forudsætter at i har løst [Øvelse 81](../exercises/81_portainer.md) 

Det er nødvendigt at tillade trafik fra opnsense på `MANAGEMENT` netværket til graylog på `MONITOR` netværket.
Dette for at tillade den data (syslog+netflow) der skal sendes fra opnsense til graylog og for at få adgang til webinterfacet på graylog.

## Instruktioner

1. Lav et firewall alias med hhv. port 514 (syslog) , port 2250 (netflow) og port 9100 (webinterface)  
    ![graylog_fw_alias](../images/opnsense_graylog_fw_alias.png)
2. Lav en firewall regel der tillader trafikken
    ![graylog_fw_log_rules](../images/opnsense_graylog_fw_log_rules.png)

## Ressourcer

- []()



