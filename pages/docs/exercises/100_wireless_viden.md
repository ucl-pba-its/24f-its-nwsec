---
hide:
  - footer
---

# Øvelse 100 - Trådløs viden

## Information

Dette er en gruppe øvelse.

Wi-Fi er trådløs netværks kommunikation. 
I denne øvelse skal i sammen undersøge nogle nøglebegreber, samt kigge lidt i de standarder der definerer Wi-Fi.

Husk at bruge nedenstående ressourcer, suppler gerne med egen research.

## Instruktioner

1. Hvad er 802.11? Forklar med egene ord.
2. Hvor mange standarder findes der pt. i 802.11 familien? 
3. Hvad er Wi-Fi?
4. Hvilke frekvenser benyttes?
5. Hvad er kanaler?
4. Hvor mange MB svarer 1000 Mb til? (hvad er formlen til at omregne?)


## Ressourcer

- [wikipedia IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11)
- [What is WiFi and How Does it Work?](https://computer.howstuffworks.com/wireless-network.htm)
- [Video: How WiFi Works - Computerphile](https://youtu.be/vvKbMueRzrI?si=wZKA2nO3nFQvqUNO)