---
hide:
  - footer
---

# Øvelse 91 - Grundlæggende ARP

## Information

Ønsker man at lave man-in-the-middle angreb på lag 2, så er ARP protokollen relevant.

I denne øvelse ser vi på hvordan protokollen virker.

## Instruktioner

1. Genopfrisk din viden om ARP [https://www.practicalnetworking.net/series/arp/traditional-arp/](https://www.practicalnetworking.net/series/arp/traditional-arp/)
2. Start Kali i vmnet8
3. Start wireshark i kali
4. Find ARP requests

    Forklar hvad der ses

5. Ping en ikke-eksisterende ip adresse og find arp requests

    Forklar hvad der ses

6. Kør `arp -e`

    Forklar hvad der ses

7. Opsamling på klassen - en gruppe præsenterer
