---
hide:
  - footer
  - toc
---

# Netværks- og kommunikationssikkerhed forår 2024

På dette website finder du ugeplaner, Øvelser, dokumenter og links til brug i faget  
*Netværks- og kommunikationssikkerhed*  

Semestrets lektionsplan finder du under [dokumenter](./other_docs/lektionsplan.md).  
[Ugeplaner](./weekly/ww_06.md) giver dig en oversigt over fagets indhold, uge for uge.  
Under [øvelser](./exercises/00_learning_goals.md) kan du se fagets øvelser. 

# Fagets indhold

Faget går ud på at forstå og håndtere netværkssikkerhedstrusler samt implementere og
konfigurere udstyr til samme.  
Faget omhandler forskelligt sikkerhedsudstyr (IDS) til monitorering.  
Derudover vurdering af sikkerheden i et netværk, udarbejdelse af plan til at lukke eventuelle sårbarheder i netværket samt gennemgang af forskellige VPN teknologier.

# Studieordning

Studieordningen er i 2 dele, en national del og en institutionel del.  

Begge dele kan findes i studiedokumenter, på ucl.dk [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

## Læringsmål fra studieordningen

### Viden

**_Den studerende har viden om og forståelse for_**  

- Netværkstrusler
- Trådløs sikkerhed
- Sikkerhed i TCP/IP
- Adressering i de forskellige lag
- Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
- Hvilke enheder, der anvender hvilke protokoller
- Forskellige sniffing strategier og teknikker
- Netværk management (overvågning/logning, snmp)
- Forskellige VPN setups
- Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI)

### Færdigheder

**_Den studerende kan_**

- Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot)
- Teste netværk for angreb rettet mod de mest anvendte protokoller
- Identificere sårbarheder som et netværk kan have.

### Kompetencer

**_Den studerende kan håndtere udviklingsorienterede situationer herunder_**

- Designe, konstruere og implementere samt teste et sikkert netværk
- Monitorere og administrere et netværks komponenter
- Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report)
- Opsætte og konfigurere et IDS eller IPS

# Eksamensdatoer

Se semesterbeskrivelsen [https://esdhweb.ucl.dk/D23-2442002.pdf](https://esdhweb.ucl.dk/D23-2442002.pdf)